<?php
session_start();
include("../config/connectordb.php");
require '../vendor/autoload.php';


use PhpOffice\PhpSpreadsheet\IOFactory;

$nombreArchivo = '../uploads/distrito.xlsx';
//se carga el archivo para leerlo
$documento = IOFactory::load($nombreArchivo);
//encontramos el total de hojas, si solo es uno, devuelve 1
$total_hojas=$documento->getSheetCount();
//total de filas
$hojaActual = $documento->getSheet(0);
//obtengo el numero de filas
$numeroFilas = $hojaActual->getHighestDataRow();

//funcion para hacer busqueda
function buscar_distrito($valor_buscar, $mysqli){
    //include("../config/connectordb.php");
    $query = "SELECT * FROM distrito_salud WHERE distrito_saludcol ='$valor_buscar';";
    $result = $mysqli->query($query);

    if(!empty($result) AND mysqli_num_rows($result) > 0){
        return 1;
        $result->free();
    }
    else{
        return 2;
        $result->free();
    }
}

for($indiceFila = 2; $indiceFila<=$numeroFilas; $indiceFila++){
    
    //lee cada fila
    $valor = $hojaActual->getCellByColumnAndRow(2,$indiceFila);
    $valor2 = $hojaActual->getCellByColumnAndRow(1,$indiceFila);
    //solo toma los que tienen contenido
    if($valor != ""){
        //aqui va el codigo para evaluar o comparar los datos no existentes
        $resultado = buscar_distrito($valor, $mysqli);
        if($resultado==2){
            echo 'El distrito de salud no existe en la bd:  '.($indiceFila).'  - '.$valor.'  - '.$valor2.'<br/>';
        }
    }
    
}
echo 'Todos los registros de distritos por area, evaluados correctamente';
session_unset();

?>