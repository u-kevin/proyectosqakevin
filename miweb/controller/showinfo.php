<?php 
	session_start();
include("../config/connectordb.php");
include("../resources/views/header.php"); 

/* require_once('../library/recaptcha/recaptchalib.php');
//  $privatekey = "6Lc3Wn0fAAAAAGJkqQIy_AajximgWi35M1_2lqeS";
//  $resp = recaptcha_check_answer ($privatekey,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);

  if (!$resp->is_valid) {
    // What happens when the CAPTCHA was entered incorrectly
    die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
         "(reCAPTCHA said: " . $resp->error . ")");
  } else {*/

if (isset($_POST['Submit'])) {
  $cui = $_POST["cui"];

  if($cui == ""){
    $_SESSION['val1']='Debe ingresar un CUI';
    ?><script><?php echo("location.href = '../index.php';");?></script><?php
    return;
  }

  if(strlen($cui)<13){
    $_SESSION['val2']='El CUI no puede ser menor a 13 caracteres';
    ?><script><?php echo("location.href = '../index.php';");?></script><?php
    return;
  }

  if(strlen($cui)>13){
    $_SESSION['val3']='El CUI no puede ser mayor a 13 caracteres';
    ?><script><?php echo("location.href = '../index.php';");?></script><?php
    return;
  }

  if(!is_numeric($cui)){
    $_SESSION['val4']='El CUI solo debe contener numeros';
    ?><script><?php echo("location.href = '../index.php';");?></script><?php
    return;
  }

  $query = "SELECT * FROM usuario WHERE cui = $cui";
  $result = $mysqli->query($query);
  $filas = mysqli_fetch_array($result,MYSQLI_ASSOC);

  if($filas==0){
    $_SESSION['val5']='El CUI ingresado no existe en la base de datos';
    ?><script><?php echo("location.href = '../index.php';");?></script><?php
    return;
  }
?>

<?php if($filas>0){?>
<div class="container">
  <div class="row">
  <div class="col-6">
    <h3 class="pt-3">Detalle usuario</h3>
    </div>
    <div class="col-6 d-flex">
    <a href="../index.php" class="btn btn-primary btn-sm ml-auto mt-3 mb-3">Regresar</a>
    </div>
  </div>
<div class="row">
  <div class="col">
  <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">CUI</th>
      <th scope="col">NOMBRES</th>
      <th scope="col">APELLIDOS</th>
      <th scope="col">TELEFONO</th>
    </tr>
  </thead>
  <tbody>

  <?php
  $query = "SELECT * FROM usuario WHERE cui = $cui";
  $result = $mysqli->query($query); 
    while ($row = $result->fetch_assoc()) {?>
      <tr>
        <th scope="row"><?php echo $row['idusuario'] ?></th>
        <td><?php echo $row['cui'] ?></td>
        <td><?php echo $row['nombres'] ?></td>
        <td><?php echo $row['apellidos'] ?></td>
        <td><?php echo $row['telefono'] ?></td>
      </tr>


    <!-- ultima linea de while -->
    <?php } ?>
    <?php $result->free();?>
  <!-- llave de cierre de if de filas mayor a 0 -->
  </tbody>
</table>
</div>
</div>
</div>
    <?php } ?>
<!-- llave de cierre para if principal -->
<?php 
include("../resources/views/footer.php");
} ?>