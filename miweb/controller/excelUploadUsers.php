<?php
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', '300');
session_start();
include("../config/connectordb.php");
include("../resources/views/header.php"); 
require '../vendor/autoload.php';

use LDAP\Result;
use PhpOffice\PhpSpreadsheet\IOFactory;

$nombreArchivo = '../uploads/usuarios.xlsx';
//se carga el archivo para leerlo
$documento = IOFactory::load($nombreArchivo);
//encontramos el total de hojas, si solo es uno, devuelve 1
$total_hojas=$documento->getSheetCount();
//total de filas
$hojaActual = $documento->getSheet(0);
//obtengo el numero de filas
$numeroFilas = $hojaActual->getHighestDataRow();
//contador
$contador=0;
$contador2=0;
$contador3=0;
$contador_bd=0;
//funcion para buscar id del distrito para realizar una insercion
function buscar_distrito($valor_buscar, $mysqli){
    $query = "SELECT iddistrito_salud FROM distrito_salud WHERE distrito_saludcol ='$valor_buscar';";
    $result = $mysqli->query($query);

    if(!empty($result) AND mysqli_num_rows($result) > 0){
        $respuesta=0;
        while ($row = $result->fetch_assoc()){

            $respuesta = $row['iddistrito_salud'];
        }
        return $respuesta;
        $result->free();
    }
    else{
        return 0;
        $result->free();
    }
}

//funcion para buscar el id de la profesion para insertar registro
function buscar_profesion($valor_buscar, $mysqli){
    $query = "SELECT idprofesion FROM profesion WHERE profesioncol ='$valor_buscar';";
    $result = $mysqli->query($query);

    if(!empty($result) AND mysqli_num_rows($result) > 0){
        $respuesta=0;
        while ($row = $result->fetch_assoc()){

            $respuesta = $row['idprofesion'];
        }
        return $respuesta;
        $result->free();
    }
    else{
        return 0;
        $result->free();
    }
}

//funcion para buscar el id del sexo
function buscar_sexo($valor_buscar, $mysqli){
    if($valor_buscar=='Mujer'){$valor_buscar='Femenino';}
    else if($valor_buscar=='Hombre'){$valor_buscar='Masculino';}
    else if($valor_buscar=='Mujer'){$valor_buscar='Femenino';}
    $query = "SELECT idsexo FROM sexo WHERE sexocol ='$valor_buscar';";
    $result = $mysqli->query($query);

    if(!empty($result) AND mysqli_num_rows($result) > 0){
        $respuesta=0;
        while ($row = $result->fetch_assoc()){

            $respuesta = $row['idsexo'];
        }
        return $respuesta;
        $result->free();
    }
    else{
        return 0;
        $result->free();
    }
}

//funcion para buscar el id de la etnia
function buscar_etnia($valor_buscar, $mysqli){
    $query = "SELECT idetnia FROM etnia WHERE etniacol ='$valor_buscar';";
    $result = $mysqli->query($query);

    if(!empty($result) AND mysqli_num_rows($result) > 0){
        $respuesta=0;
        while ($row = $result->fetch_assoc()){

            $respuesta = $row['idetnia'];
        }
        return $respuesta;
        $result->free();
    }
    else{
        return 0;
        $result->free();
    }
}


//funcion para insertar un registro
function insertar_usuario($id, $cui, $nombres, $apellidos, $telefono, $distrito, $profesion,$sexo,$etnia,$mysqli){
    $query = "INSERT INTO `usuario` (`idusuario`, `cui`, `nombres`, `apellidos`, `telefono`, `distrito_salud_iddistrito_salud`, `profesion_idprofesion`, `sexo_idsexo`, `etnia_idetnia`)
    VALUES ('$id', '$cui', '$nombres', '$apellidos', '$telefono', '$distrito', '$profesion', '$sexo', '$etnia')";
    $result = $mysqli->query($query);
    if($result){
        return 1;
        $result->free();
    }
    else{
        return 0;
        $result->free();
    }
}

//funcion para buscar el id de la etnia
function buscar_cui_duplicado($cui, $numeroFilas,$hojaActual){
    $contador_interno=0;
    $indice = 0;
    $aux_indice=0;
    $bandera=true;
    $bandera2=false;
    $bandera3=false;

    for($indiceFila = 2; $indiceFila<=$numeroFilas; $indiceFila++){
        $cui2 = $hojaActual->getCellByColumnAndRow(1,$indiceFila);


        if($cui2 != ""){
            if($cui2==$cui){
                $contador_interno++;
                if($contador_interno==1){
                    $aux_indice=$indiceFila;
                }
                else{
                    $bandera2=true;
                    $indice=$indiceFila;
                }
            }

            if($contador_interno>1)
            {
                if($bandera){
                    echo 'EL REGISTRO UBICADO EN LA LINEA:  '.($aux_indice).'<br/>';
                    $bandera=false;
                }
                else if($bandera2){
                    echo '--------------Se repite en la linea:  '.($indice).'<br/>';
                    $bandera2=false;
                    $bandera3=true;
                }
            }
        }
    }
    return $bandera3;
}


?>

<div class="container">
  <div class="row">
  <div class="col-6">
    <h3 class="pt-3">Usuarios</h3>
    </div>
    <div class="col-6 d-flex">
    <a href="../index.php" class="btn btn-primary btn-sm ml-auto mt-3 mb-3">Regresar</a>
    </div>
  </div>
<div class="row">
  <div class="col">
  <table class="table">
  <thead class="thead-dark">
    <!-- <tr>
      <th scope="col">ID</th>
      <th scope="col">CUI</th>
      <th scope="col">NOMBRES</th>
      <th scope="col">APELLIDOS</th>
      <th scope="col">TELEFONO</th>
      <th scope="col">DISTRITO SALUD</th>
      <th scope="col">PROFESION</th>
      <th scope="col">SEXO</th>
      <th scope="col">ETNIA</th>
    </tr> -->
  </thead>
  <tbody>

<?php
//Mostrar cada uno de los registros recuperados
for($indiceFila = 2; $indiceFila<=$numeroFilas; $indiceFila++){
    
    //lee cada fila del excel
    $id = $indiceFila-1;
    $cui = $hojaActual->getCellByColumnAndRow(1,$indiceFila);
    $nombres = $hojaActual->getCellByColumnAndRow(2,$indiceFila);
    $apellidos = $hojaActual->getCellByColumnAndRow(3,$indiceFila);
    $telefono = $hojaActual->getCellByColumnAndRow(6,$indiceFila);
    //Se consultan los ids tanto del tipo de evento como de la modalidad
    $aux_distrito = $hojaActual->getCellByColumnAndRow(8,$indiceFila);
    $aux_profesion = $hojaActual->getCellByColumnAndRow(9,$indiceFila);
    $aux_sexo = $hojaActual->getCellByColumnAndRow(4,$indiceFila);
    $aux_etnia = $hojaActual->getCellByColumnAndRow(5,$indiceFila);
    

    //busquedas de ids 
    $distrito=buscar_distrito($aux_distrito,$mysqli);
    $profesion=buscar_profesion($aux_profesion,$mysqli);
    $sexo=buscar_sexo($aux_sexo,$mysqli);
    $etnia=buscar_etnia($aux_etnia, $mysqli);

    //identificacion cuis duplicados
    if($nombres != ""){
        if($cui != ""){
            $res = buscar_cui_duplicado($cui, $numeroFilas, $hojaActual);
            if(!$res){
                //ACA DEBE IR LA INSERCION EN LA BD
                //se insertan los distintos registros
                $respuesta = insertar_usuario($id,$cui,$nombres,$apellidos,$telefono,$distrito,$profesion,$sexo,$etnia,$mysqli);
                if($respuesta){$contador++;}
                else{
                    echo 'El registro ubicado en la linea:  '.($indiceFila).'<br/>';
                    echo '-----------------------------------------------------------------------No se inserto en la bd, por algun fallo<br/>';
                }
            }
            else{
                $contador3++;
            }
        }
        else{
            echo 'El registro ubicado en la linea:  '.($indiceFila).'<br/>';
            echo '-------------------------No tiene CUI, por lo que no se puede insertar en la bd<br/>';
            $contador2++;
        }

        
    ?>
    
<?php
        //se insertan los distintos registros
        // $res = insertar_evento($id, $evento, $fecha_inicio, $fecha_final, $objetivo, $tipo, $modalidad,$mysqli);
        // if($res){}
        // else{$contador++;}
    }    
}
?>
  </tbody>
</table>
</div>
</div>
</div>
<?php 

// validaciond de datos cargados a la bd
if($contador>0){
    echo '<br/> SE HAN CARGADO A LA BASE DE DATOS --> '.($contador).' REGISTROS<br/>';
    echo '<br/> SIN CARGAR A LA BASE DE DATOS (NO CUENTA CON CUI) --> '.($contador2).' REGISTROS<br/>';
    echo '<br/> SIN CARGAR A LA BASE DE DATOS  (CUI DUPLICADO) --> '.($contador3).' REGISTROS<br/>';
}
else{
    echo '<br/> NO SE HA CARGADO NINGUN REGISTRO A LA BASE DE DATOS<br/>';
}

session_unset();
include("../resources/views/footer.php");
?>