<?php
session_start();
include("../config/connectordb.php");
include("../resources/views/header.php"); 
require '../vendor/autoload.php';

use LDAP\Result;
use PhpOffice\PhpSpreadsheet\IOFactory;

$nombreArchivo = '../uploads/evento.xlsx';
//se carga el archivo para leerlo
$documento = IOFactory::load($nombreArchivo);
//encontramos el total de hojas, si solo es uno, devuelve 1
$total_hojas=$documento->getSheetCount();
//total de filas
$hojaActual = $documento->getSheet(0);
//obtengo el numero de filas
$numeroFilas = $hojaActual->getHighestDataRow();
//contador
$contador=0;


//funcion para buscar id de tipo de evento
function buscar_evento($valor_buscar, $mysqli){
    $query = "SELECT idtipo_evento FROM tipo_evento WHERE tipo_eventocol ='$valor_buscar';";
    $result = $mysqli->query($query);

    if(!empty($result) AND mysqli_num_rows($result) > 0){
        $respuesta=0;
        while ($row = $result->fetch_assoc()){

            $respuesta = $row['idtipo_evento'];
        }

        return $respuesta;

        $result->free();
    }
    else{
        return 0;
        $result->free();
    }
}

//funcion para buscar el id de la modalidad para insertar registro
function buscar_modalidad($valor_buscar, $mysqli){
    $query = "SELECT idmodalidad FROM modalidad WHERE modalidadcol ='$valor_buscar';";
    $result = $mysqli->query($query);

    if(!empty($result) AND mysqli_num_rows($result) > 0){
        $respuesta=0;
        while ($row = $result->fetch_assoc()){

            $respuesta = $row['idmodalidad'];
        }

        return $respuesta;

        $result->free();
    }
    else{
        return 0;
        $result->free();
    }
}

//funcion para insertar un registro
function insertar_evento($id, $evento, $fecha1, $fecha2, $objetivo, $tipo, $modalidad,$mysqli){
    $query = "INSERT INTO `evento` (`idevento`, `eventocol`, `fecha_inicio`, `fecha_fin`, `objetivo`, `tipo_evento_idtipo_evento`, `modalidad_idmodalidad`)
    VALUES ('$id', '$evento', '$fecha1', '$fecha2', '$objetivo', '$tipo', '$modalidad')";
    $result = $mysqli->query($query);
    if($result){
        return 1;
        $result->free();
    }
    else{
        return 0;
        $result->free();
    }
}


?>

<div class="container">
  <div class="row">
  <div class="col-6">
    <h3 class="pt-3">Eventos</h3>
    </div>
    <div class="col-6 d-flex">
    <a href="../index.php" class="btn btn-primary btn-sm ml-auto mt-3 mb-3">Regresar</a>
    </div>
  </div>
<div class="row">
  <div class="col">
  <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">EVENTO</th>
      <th scope="col">FECHA INICIO</th>
      <th scope="col">FECHA FIN</th>
      <th scope="col">OBJETIVO</th>
      <th scope="col">TIPO DE EVENTO</th>
      <th scope="col">MODALIDAD</th>
    </tr>
  </thead>
  <tbody>

<?php
//Mostrar cada uno de los registros recuperados
for($indiceFila = 2; $indiceFila<=$numeroFilas; $indiceFila++){
    
    //lee cada fila del excel
    $id = $indiceFila-1;
    $evento = $hojaActual->getCellByColumnAndRow(1,$indiceFila);
    $fecha_inicio = $hojaActual->getCellByColumnAndRow(2,$indiceFila);
    $fecha_final = $hojaActual->getCellByColumnAndRow(3,$indiceFila);
    $objetivo = $hojaActual->getCellByColumnAndRow(6,$indiceFila);
    //Se consultan los ids tanto del tipo de evento como de la modalidad
    $aux_tipo = $hojaActual->getCellByColumnAndRow(4,$indiceFila);
    $aux_modalidad = $hojaActual->getCellByColumnAndRow(5,$indiceFila); 
    $tipo=buscar_evento($aux_tipo,$mysqli);
    $modalidad=buscar_modalidad($aux_modalidad,$mysqli);
    if($evento != ""){

    ?>

    <tr>
        <th scope="row"><?php echo $id ?></th>
        <td><?php echo $evento ?></td>
        <td><?php echo $fecha_inicio ?></td>
        <td><?php echo $fecha_final ?></td>
        <td><?php echo $objetivo ?></td>
        <td><?php echo $tipo ?></td>
        <td><?php echo $modalidad ?></td>
      </tr>
    
<?php
        //se insertan los distintos registros
        $res = insertar_evento($id, $evento, $fecha_inicio, $fecha_final, $objetivo, $tipo, $modalidad,$mysqli);
        if($res){}
        else{$contador++;}
    }    
}
?>
  </tbody>
</table>
</div>
</div>
</div>
<?php 

// validaciond de datos cargados a la bd
if($contador>0){
    echo "No se cargaron los datos correctamente a la Base De Datos";
}
else{
    echo "Se cargaron los datos correctamente a la Base De Datos";
}

session_unset();
include("../resources/views/footer.php");
?>